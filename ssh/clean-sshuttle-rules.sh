#!/usr/bin/env bash
#
# clean-sshuttle-rules.sh
# Copyright 2020 eth0 <ethernet.zero@gmail.com>
#
# This work is free. You can redistribute it and/or modify it under the terms of
# the ISC License.
#

if [[ "$(id -u)" != 0 ]]; then
	exec sudo "$0" "$@"
fi

[[ "$1" == noop ]] && noop=1

readarray -t rules < <(
	iptables-save \
		| grep 'j sshuttle' \
		| grep -vf <(ss -plant | awk '/sshuttle/{split($4, a, ":"); print "sshuttle-" a[2]}') \
		| sed 's/^-A/-D/g'
)

echo "Cleaning ${#rules[@]} iptables rules${noop+ (not really)}"

if [[ -z "$noop" ]]; then
	(( ${#rules[@]} > 0 )) && echo
	for r in "${rules[@]}"; do
		echo "$r"
		iptables -t nat $r
	done
	(( ${#rules[@]} > 0 )) && echo
fi

readarray -t chains < <(
	iptables-save \
		| grep ':sshuttle' \
		| awk '{print $1}' \
		| cut -d: -f2 \
		| grep -vf <(ss -plant | awk '/sshuttle/{split($4, a, ":"); print "sshuttle-" a[2]}')
)

echo "Removing ${#chains[@]} iptables chains${noop+ (not really)}"

if [[ -z "$noop" ]]; then
	(( ${#chains[@]} > 0 )) && echo
	for i in "${chains[@]}"; do
		echo "$i"
		iptables -t nat -F $i
		iptables -t nat -X $i
	done
	(( ${#chains[@]} > 0 )) && echo
fi
