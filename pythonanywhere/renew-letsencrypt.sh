#!/usr/bin/env bash
#
# renew-letsencrypt.sh
# Copyright 2018-2019 eth0 <ethernet.zero@gmail.com>
# 
# This work is free. You can redistribute it and/or modify it under the terms of
# the ISC License. See the COPYING file for more details.
#

mail_from=your-email@example.com
mail_to=support@pythonanywhere.com

domain="$1"

expiration="$(date -d "$(openssl x509 -enddate -noout -in "${HOME}/letsencrypt/certs/${domain}/cert.pem" | cut -d= -f2)" +'%s')"
threshold="$(date -d '+30 days' +'%s')"

(( threshold < expiration )) && exit 0

~/dehydrated/dehydrated \
	--cron \
	--config "${HOME}/letsencrypt/config/${domain}.conf" \
	--domain "$domain" \
	--out "${HOME}/letsencrypt/certs" \
	--challenge http-01

if (($? == 0)); then
	ssmtp -C ~/.ssmtprc "${mail_to}" <<-EOF
	From: ${mail_from}
	Subject: Please update the SSL certificate for ${domain}

	Hi!

	Could you please update the SSL certificate for my webapp?

	This is the data you need:

	    User name: ${USER}
	    Domain: ${domain}
	    Path to certificates: ${HOME}/letsencrypt/certs/${domain}
	EOF
fi
