#!/usr/bin/env bash
#
# gcloud-rdp.sh
# Copyright 2019 eth0 <ethernet.zero@gmail.com>
# 
# This work is free. You can redistribute it and/or modify it under the terms of
# the ISC License. See the COPYING file for more details.
#

[[ -n "$2" ]] && project="$(gcloud projects list --filter="project_id:('*${2}*')" --format="value(project_id)")"
[[ -z "$project" ]] && project="$(gcloud projects list --format="value(project_id)" | head -n 1)"

instance_name="$1"
gcloud_user="$(gcloud auth list --filter="active" --format="value(account.split('@').slice(0))")"
gcloud_user="$(sed 's/[^a-z0-9]/_/g' <<< "$gcloud_user")"
instance_data=()
read -ra instance_data < <(gcloud compute --project "$project" instances list --filter="name=$instance_name" --format="value(name,networkInterfaces[0].networkIP,zone.scope())")

instance_pass="$(gcloud compute --project "$project" reset-windows-password "$instance_name" --zone "${instance_data[2]}" --quiet 2>/dev/null | awk '/^password:/{print $2}')"

cat <<EOF
Opening RDP connection:

    User: $gcloud_user
    Instance: $instance_name
    Address: ${instance_data[1]}
    Zone/region: ${instance_data[2]}

EOF

xfreerdp +clipboard +fonts /gdi:hw /u:"$gcloud_user" /dynamic-resolution -grab-keyboard /p:"$instance_pass" /v:"${instance_data[1]}"
