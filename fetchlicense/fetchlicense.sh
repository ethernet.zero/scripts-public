#!/usr/bin/env bash
#
# fetchlicense.sh
# Copyright 2019 eth0 <ethernet.zero@gmail.com>
# 
# This work is free. You can redistribute it and/or modify it under the terms of
# the ISC License. See the COPYING file for more details.
#

detect_comments()
{
	local language="$1"

	case "$language" in
		sh|py|rb|pl|ps1)
			echo -ne '#\n# \n#'
			;;
		lua|sql)
			echo -ne '--\n-- \n--'
			;;
		c|cc|cpp|C|h|hpp|H|java|go|php|cs|groovy|css|js|rs)
			echo -ne '/*\n * \n */'
			;;
		htm|html|xml|dtd|xsl|xslt|xsd)
			echo -ne '<!--\n  -- \n  -->'
			;;
		vimrc|gvimrc|vim)
			echo -ne '"\n" \n"'
			;;
		*)
			echo -ne '\n\n'
			;;
	esac
}

extract_license_json_field()
{
	local in=''
	if read -r -t 0 _; then
		in="$(cat)"
	fi
	if [[ "$(command -v jq)" ]]; then
		jq -r --arg field "$1" '.[$field]' <<< "$in" 2>/dev/null
	else
		out="$(grep -Eo "\"$1\": *\"[^\"]*\"" <<< "$in")"
		out=${out%\"*}  # "
		out=${out#*:}
		out=${out#*\"}  # "
		builtin printf -- "$out"
	fi
}

license_id="${1:-$(git config --get license.id 2>/dev/null)}"

repo_first_year="$(git log "$(git rev-list --max-parents=0 HEAD 2>/dev/null)" --pretty=format:%ad --date=format:%Y 2>/dev/null)"
copyright_year="${repo_first_year:-$(date +%Y)}"

author_name="$(git config --get user.name 2>/dev/null)"
author_email="$(git config --get user.email 2>/dev/null)"
author="${author_name:-Awesome Coder} <${author_email:-awesome.coder@example.com}>"

if [[ "$copyright_year" =~ ^[0-9]+$ ]] && (( copyright_year < $(date +%Y) )); then
	copyright_year+="-$(date +%Y)"
fi

license_json="$(curl -sfL "https://spdx.org/licenses/${license_id:-MIT}.json" 2>/dev/null)"

if [[ -z "$license_json" ]]; then
	cat >&2 <<-EOF
	Couldn't fetch a license with the ID '${license_id}'. Are you sure it's correct?
	Take a look at the list at https://spdx.org/licenses/ if you're in doubt.
	EOF
	exit 1
fi

license_name="$(extract_license_json_field name <<< "$license_json")"
license_text="$(extract_license_json_field licenseText <<< "$license_json" | fmt -w 80)"

boilerplate_ending=', reproduced below:'
(( $# > 1 )) && boilerplate_ending=$'. See the COPYING\nfile for more details.'

boilerplate=$(fmt -w 80 <<-EOF
Copyright ${copyright_year} ${author}

This work is free. You can redistribute it and/or modify it under the
terms of the ${license_name}${boilerplate_ending}
EOF
)

(( $# > 1 )) && {
	languages=("${@:2}")
	for f in "${languages[@]}"; do
		comments=()
		readarray -t comments < <(detect_comments "$f")
		readarray -t boilerplate_lines <<< "$boilerplate"
		cat <<-EOF
		${comments[0]}
		$(builtin printf -- "${comments[1]}%s\\n" "${boilerplate_lines[@]}")
		${comments[2]}
		EOF
	done
	exit 0
}

cat <<-EOF
${boilerplate}

${license_text}
EOF
