#!/usr/bin/env bash
#
# add-kubeconfig.sh
# Copyright 2020 eth0 <ethernet.zero@gmail.com>
#
# This work is free. You can redistribute it and/or modify it under the terms of
# the ISC License.
#

yq="$(command -v yq)"

if [[ -z "$yq" ]]; then
	echo "$0: you need the yq tool, download it from https://github.com/mikefarah/yq/releases" >&2
	exit 1
fi

if [[ ! "$1" ]]; then
	echo "$0: you must specify a context name" >&2
	exit 1
fi

context_name="$1"
kcfile="${2-admin.conf}"
kubeconfig=${3-~/.kube/config}

if [[ ! -f "$kcfile" ]]; then
	echo "$0: missing cluster config file" >&2
	exit 1
fi

if [[ ! -f "$kubeconfig" ]]; then
	mkdir -p "$(cd "$(dirname "$kubeconfig")"; pwd)"
fi

kctemp="$(< "$kcfile")"

# First, clean the cluster config file
if [[ -f "$kubeconfig" ]]; then
	kctemp="$("$yq" eval "del(.[\"apiVersion\", \"current-context\", \"kind\", \"preferences\"])" - <<< "$kctemp")"
fi

# Set the context name for all entries
kcexpr="$(printf '"%s" as $contextname |\n' "$context_name"; cat <<-'EOF'
. * {
	"contexts": [.contexts[] | (.context | (.cluster,.user),.name) |= $contextname],
	"users": [.users[] | .name |= $contextname],
	"clusters": [.clusters[] | .name |= $contextname]
}
EOF
)"
kctemp="$("$yq" eval "$kcexpr" - <<< "$kctemp")"

# Then, merge the cleaned config to the kubeconfig file
if [[ -f "$kubeconfig" ]]; then
	kcexpr="$(printf '"%s" as $contextname |\n' "$context_name"; cat <<-'EOF'
	(
		select(fileIndex == 0) |
		del(.["contexts", "users", "clusters"][] | select(.name == $contextname))
	) *+d select(fileIndex == 1)
	EOF
	)"
	"$yq" -i eval-all "$kcexpr" "$kubeconfig" - <<< "$kctemp"
else
	cat > "$kubeconfig" <<< "$kctemp"
fi
