#!/usr/bin/env bash
#
# extract-kubeconfig.sh
# Copyright 2020 eth0 <ethernet.zero@gmail.com>
#
# This work is free. You can redistribute it and/or modify it under the terms of
# the ISC License.
#

yq="$(command -v yq)"

if [[ -z "$yq" ]]; then
	echo "$0: you need the yq tool, download it from https://github.com/mikefarah/yq/releases" >&2
	exit 1
fi

if [[ ! "$1" ]]; then
	echo "$0: you must specify a context name" >&2
	exit 1
fi

context_name="$1"
kubeconfig=${2-~/.kube/config}

if [[ ! -f "$kubeconfig" ]]; then
	echo "$0: $kubeconfig doesn't exist" >&2
	exit 1
fi

kcexpr="$(printf '"%s" as $contextname |\n' "$context_name"; cat <<-'EOF'
(.contexts[] | select(.name == $contextname).context) as $context |
{
	"apiVersion": "v1",
	"kind": "Config",
	"preferences": {},
	"current-context": $contextname,
	"contexts": [{"name": $contextname, "context": $context}],
	"users": [.users[] | select(.name == $context.user)],
	"clusters": [.clusters[] | select(.name == $context.cluster)]
}
EOF
)"

yq eval "$kcexpr" "$kubeconfig"
