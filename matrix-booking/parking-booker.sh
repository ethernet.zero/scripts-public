#!/usr/bin/env bash

# Join an array using a separator
join()
{
	local sep="$1"
	shift

	local ret
	printf -v ret "%s$sep" "$@"
	echo "${ret::-${#sep}}"
}

# Call the Matrix Booking API
api()
{
	local api='https://app.matrixbooking.com/api/v1'
	local endpoint="$1"
	shift
	# Join query parameters
	local params="$(join '&' "$@")"
	local curl_args=(-snL)

	[[ "$endpoint" != /* ]] && endpoint="/$endpoint"

	# If you send something through stdin, then it's a POST request
	if read -t0 _; then
		curl_args+=(
			-X POST
			-H 'Content-Type: application/json;charset=utf-8'
			-H 'X-Time-Zone: Europe/Madrid'
			-d@-
		)
	fi

	curl "${curl_args[@]}" "$api$endpoint${params+?}${params}"
}

# Shortcut function for jq'ing
json()
{
	local query="$1"
	shift

	jq -rc "$@" "$query" 2>/dev/null
}

# Make sure all necessary options are provided
ensure_opts()
{
	for arg; do
		[[ "${!arg}" ]] || fail "You need to set the $arg option in $cfgfile or provide the --${arg//_/-}=<${arg^^}> option in the commandline"
	done
}

# Override variables from long options passed as arguments
# For example, --variable-name=value would set variable_name='value'
parse_args()
{
	local _opt_
	local _val_
	local _arg_
	for _arg_; do
		if [[ "$_arg_" == --* ]]; then
			_opt_="${_arg_#--}"
			_val_=''
			[[ "$_opt_" == *=* ]] && _val_="${_opt_#*=}"
			_opt_="${_opt_%%=*}"
			_opt_="${_opt_//-/_}"
			declare -n _var_="$_opt_"
			_var_="$_val_"
			unset -n _var_
		fi
	done
}

# Fatal error
fail()
{
	echo "${0##*/}: $*" >&2
	exit 1
}

# Check if a command is a file on disk and not an alias or a function
is_cmd()
{
	local command="$1"
	[[ "$(type -p "$command" 2>/dev/null)" ]]
}

# Portable-ish GNU+BSD date command
_date()
{
	local datespec="$1"
	local date
	local retval

	if [[ "$OSTYPE" == *darwin* ]]; then
		# If it's a Mac and Homebrew is installed, install coreutils to get
		# GNU date as gdate
		if ! is_cmd gdate && is_cmd brew; then
			echo 'Ah, let me install GNU date for you.'
			brew install coreutils || fail "Couldn't install GNU date in your system"
		fi
		date="$(gdate -d "$datespec" +%F 2>/dev/null)"
		retval=$?
	elif ! is_cmd date; then
		# No date command in your system? How odd!
		fail "There's no date command in your system"
	else
		# Try a GNU date calculation
		date="$(date -d "$datespec" +%F 2>/dev/null)"
		retval=$?

		# If it doesn't generate anything, date is probably a BSD date
		# It can be a macOS without Homebrew or any other *BSD
		if [[ -z "$date" ]]; then
			# If the booking date is the default, try the BSD date equivalent
			# Unfortunately we can't translate GNU strtotime() values automatically
			# because those date specs can be complex
			[[ "$datespec" == '+2 weeks' ]] && datespec='+2w'

			# Now try a BSD date command line
			date="$(date -v"$datespec" +%F 2>/dev/null)"
			retval=$?
		fi

		# Nothing generated with a GNU nor a BSD date command line, so whatever
		# date command is in the system, we don't know how to use it
		[[ "$date" ]] || retval=1
	fi

	echo "$date"
	return "$retval"
}

booking_date='+2 weeks'

cfgfile="${XDG_CONFIG_HOME:-$HOME/.config}/matrix-booker.conf"

# Import all variable definitions in the config file, which means you can have
# all other sorts of comments and lines in it and they will be ignored
[[ -r "$cfgfile" ]] && source <(grep -E '^[[:alnum:]_]+=' "$cfgfile")
# Then override any variables passed as long options in the command line
(( $# )) && parse_args "$@"

# After that, make sure none of the necessary variables have been assigned an
# empty value
ensure_opts parking_area booking_date spot_name license_plate

# Get user data from Matrix Booking
userdata="$(api /user/current)"
if [[ "$userdata" == *'doctype html'* ]]; then
	fail "Login failed, please make sure to set your credentials in your ~/.netrc for app.matrixbooking.com"
fi
owner="$(json '{id: .personId, email: .email, name: .name}' <<< "$userdata")"

# Get building ID
parking_area_id="$(api /location kind=BUILDING | \
	json '.[] | select(.name == $parking_area) | .id' \
	--arg parking_area "$parking_area")"

if [[ -z "$parking_area_id" || "$parking_area_id" == null ]]; then
	fail "Parking area '$parking_area' not found"
fi

# Get spot ID
spot="$(api /location kind=EQUIPMENT l="$parking_area_id" | \
	json '.[] | select(.name == $spot_name) | .id' \
	--arg spot_name "$spot_name")"

if [[ -z "$spot" || "$spot" == null ]]; then
	fail "Parking spot '$spot_name' not found"
fi

date="$(_date "$booking_date")"

if [[ -z "$date" ]]; then
	fail "Invalid booking date '$booking_date'"
fi

echo "Booking parking spot '$spot_name' on $date..."

response="$(api /booking notifyScope=ALL_ATTENDEES <<-JSON
{
  "timeFrom": "${date}T09:00:00.000",
  "timeTo": "${date}T20:00:00.000",
  "locationId": $spot,
  "label": "$license_plate",
  "attendees": [],
  "extraRequests": [],
  "bookingGroup": {},
  "owner": $owner,
  "ownerIsAttendee": true
}
JSON
)"

if [[ "$response" == *'doctype html'* ]]; then
	fail "Error when performing the API request to book your parking spot"
elif [[ "$(json '.messageCode' <<< "$response")" == error* ]]; then
	fail "$(json '.message' <<< "$response")"
else
	echo "Your booking for parking spot '$spot_name' on $date is $(json '.status' <<< "$response")."
fi
